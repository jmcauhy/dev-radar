const express = require('express'),
      mongoose = require('mongoose'),
      cors = require('cors'),
      http = require('http'),
      routes = require('./routes'),
      { setupWebsocket } = require('./websocket')

const app = express()

const server = http.Server(app)

setupWebsocket(server)

mongoose.connect('mongodb+srv://jmcauhy:Access601599@cluster0-tcgkc.mongodb.net/omnistack', {
   useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true
})

app.use(cors({ origin: 'http://localhost:3000' }))

app.use(express.json())

app.use(routes)

server.listen(3333)