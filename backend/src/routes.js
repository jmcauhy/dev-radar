const { Router } = require('express')

const DevController = require('./controllers/DevController'),
      SearchController = require('./controllers/SearchController')
    
const routes = Router()

routes.get('/search', SearchController.index)

routes.get('/devs', DevController.index)
routes.post('/devs', DevController.store)
routes.put('/devs', DevController.update)
routes.delete('/devs', DevController.delete)

module.exports = routes