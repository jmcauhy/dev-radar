const axios = require('axios'),
      Dev = require('../models/Dev'),
      parseStringAsArray = require('../utils/parseStringAsArray')

module.exports = {
    // Listar devs
    async index(req, res) {
        const devs = await Dev.find()

        return res.json(devs)
    },

    // Cadastrar dev
    async store(req, res) {
        const { github_username, techs, latitude, longitude } = req.body

        let dev = await Dev.findOne({ github_username })

        if (!dev) {
            const apiResponse = await axios.get(`https://api.github.com/users/${github_username}`)
    
            const { name = login, avatar_url, bio } = apiResponse.data
        
            const techsArray = parseStringAsArray(techs)
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude]
            }
        
            dev = await Dev.create({
                github_username,
                name,
                avatar_url,
                bio,
                techs: techsArray,
                location
            })
        }
    
        return res.json(dev)
    },

    // Atualizar dev
    async update(req, res) {
        const { github_username, avatar_url, name, techs, bio, latitude, longitude } = req.body

        let dev = await Dev.findOne({ github_username })

        if (dev) {
            const techsArray = parseStringAsArray(techs)
        
            const location = {
                type: 'Point',
                coordinates: [longitude, latitude]
            }
        
            dev = await Dev.updateOne({ github_username }, {
                name,
                avatar_url,
                bio,
                techs: techsArray,
                location
            })
        }

        return res.json(dev)
    },

    // Deletar dev
    async delete(req, res) {
        const { github_username } = req.query

        const dev = await Dev.deleteOne({ github_username })

        return res.json(dev)
    }
}