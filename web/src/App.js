import React, { useState, useEffect } from 'react'
import api from './services/api'

import './global.css'
import './App.css'
import './Sidebar.css'
import './Main.css'

import DevForm from './components/DevForm'
import './components/DevForm/styles.css'

import DevItem from './components/DevItem'
import './components/DevItem/styles.css'

function App() {
	const [devs, setDevs] = useState([])

	async function loadDevs() {
		const response = await api.get('/devs')

		setDevs(response.data)
	}

	async function handleAddDev(data) {
		const response = await api.post('/devs', data)
		
		setDevs([...devs, response.data])
	}

	async function handleDelDev(data) {
		await api.delete('/devs', { params: {github_username: data} })
	
		setDevs([...devs])
	}

	loadDevs()

	return (
		<div id="app">
			<aside>
				<strong>Cadastro</strong>
				<DevForm onSubmit={handleAddDev} />
			</aside>

			<main>
				<ul>
					{devs.map(dev => (
						<DevItem key={dev._id} dev={dev} onClick={handleDelDev} />
					))}
				</ul>
			</main>
		</div>
	);
}

export default App;