import React from 'react'

function DevItem({ dev, onClick }) {
    async function handleClick() { await onClick(dev.github_username) }

    return (
        <li className="dev-item">
            <header>
                <img src={dev.avatar_url} alt={dev.name} />
                <div className="user-info">
                    <strong>{dev.name}</strong>
                    <span>{dev.techs.join(', ')}</span>
                </div>
            </header>
            <p>{dev.bio}</p>
            <div className="links">
                <a
                    href={`https://github.com/${dev.github_username}`} target="_blank"
                    rel="noopener noreferrer">
                    Acessar Github
                </a>

                <button onClick={handleClick} >Excluir</button>
            </div>
        </li>
    )
}

export default DevItem