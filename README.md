<div align="center">
    <img alt="Be The Hero" src="./web/assets/devradar.svg" />
</div>
<br/>
<div align="center">A project developed during the 10th Omnistack week, by [Rocketseat](https://github.com/rocketseat/)</div>
<br/>
<hr/>
<br/>
<div align="center">
    <img alt="Dev Radar" src="https://i.imgur.com/hcoax4W.png" />
</div>

## :clipboard: Description

Dev Radar was developed to connect developers near each other that work with the same technologies. The app works by colecting data from the register, the Github API and the expo-location API, displaying the user in matching searchs. The matches are based on their estimated location and technologies they work with.

## :computer: Technologies
This project was developed using [React](https://reactjs.org/), 
[React Native](https://facebook.github.io/react-native/) (with [Expo](https://expo.io/)), 
[Node.js](https://nodejs.org/en/) and [MongoDB](https://www.mongodb.com/) (with [express](https://expressjs.com/pt-br/) and [mongoose](https://mongoosejs.com/)).

## :bulb: How to contribute

- Fork this repository;
- Create a branch with your feature: `git checkout -b my-feature`;
- Commit your changes: `git commit -m 'My new feature'`;
- Push to your branch: `git push origin my-feature`.

## :scroll: License

This project is under the [MIT](https://choosealicense.com/licenses/mit/) license.